include <dotSCAD/src/helix.scad>;
include <dotSCAD/src/shape_cyclicpolygon.scad>;
include <dotSCAD/src/arc_path.scad>;
include <dotSCAD/src/rotate_p.scad>;
include <dotSCAD/src/cross_sections.scad>;
include <dotSCAD/src/polysections.scad>;
include <dotSCAD/src/polyline2d.scad>;
include <dotSCAD/src/polyline3d.scad>;
include <dotSCAD/src/line2d.scad>;
include <dotSCAD/src/line3d.scad>;
include <dotSCAD/src/arc.scad>;
include <dotSCAD/src/helix_extrude.scad>;
include <dotSCAD/src/path_extrude.scad>;
include <dotSCAD/src/hollow_out.scad>;
include <dotSCAD/src/along_with.scad>;
include <dotSCAD/src/bend.scad>;

// Some parameters for better circles
$fa = $preview ? 20 : 6;
$fs = $preview ? 1 : 0.075;

function ring(n) = [0 : round(360 / n) : 360];

function reverse(n) = [ for (a=[len(n)-1:-1:0]) n[a]];

module mirror_copy(v=[0, 1, 0]) {
    mirror(v=v) children();
    children();
};

module translate_cut(v=[0, 0, 0]) {
    projection(cut=true)
    translate(v)
    children();
};


module mirror_bend(v=[0, 1, 0], size, angle, frags, center=false) {
    rotate([-90, 0, 0])
    translate([0, size.y / (2 * sin (angle/2)) - size.z * sin(angle), center ?  -size.y/2 : 0])
        rotate([0, 0, -angle/2]) bend(size, angle, frags)
        translate([size.x/2, 0, 0]) mirror_copy(v=v) children();
};

module fillet_join(r=1, v=[0, 0, 0]) {
    // Let's make a difference between the projection of our two children
    // Then a minkowski of that with a sphere
    // And finally a difference with an offseted projection
    // of the second child
    // To be sure to keep the fillet inside the first children, we're 
    // intersecting it with a projection of this child
    $fn = 10;
    children(0);
    translate(v)
    intersection() {
        linear_extrude(r)
        translate_cut(v=-v) children(0);
        difference() {
            linear_extrude(r)
            offset(r=r)
            translate_cut(v=-v) children(1);
            translate([0, 0, r])
            minkowski() {
                linear_extrude(r)
                difference() {
                    translate_cut(v=-v) children(0);
                    offset(r=r)
                    translate_cut(v=-v) children(1);
                };
                sphere(r=r);
            };
        };
    };
    children(1);
};
    //intersection() {
    //    linear_extrude(r)
    //    offset(r=r)
    //    projection(cut=true)
    //    translate(-v)
    //    children(1);
    //    difference() {
    //        linear_extrude(r)
    //        offset(r=r)
    //        projection(cut=true)
    //        translate(-v)
    //        children(1);
    //        minkowski() {
    //            linear_extrude(r)
    //            difference() {
    //                projection(cut=true)
    //                translate(-v)
    //                children(0);
    //                projection(cut=true)
    //                translate(-v)
    //                children(1);
    //            };
    //            sphere(r=r);
    //        };
    //    };
    //};
    //union() {
    //    children(0);
    //    translate(v)
    //    linear_extrude(r)
    //    offset(r=r)
    //    projection(cut=true)
    //    translate(-v)
    //    children(1);
    //    children(1);
    //};
//};

module sphere_joint_out(radius=5, heigth=1, cutout=0) {
    difference() {
        union() {
            difference() {
                sphere(r=radius+heigth);
                translate([0, 0, 2*(radius+heigth)-cutout]) cube([2*(radius+heigth), 2*(radius+heigth), 2*(radius+heigth)], center=true);
            };
            children();
        };
        sphere(r=radius);
        translate([0, 0, -0.75*(radius+heigth)]) cube([2*(radius+heigth), 2*(radius+heigth), radius+heigth], center=true);
    };
};

module sphere_joint_in(radius=5, heigth=1) {
    difference() {
        sphere(r=radius);
        sphere(r=radius-heigth);
        cylinder(r=heigth, h=radius);
        translate([0, 0, radius+heigth]) sphere(r=heigth*2);
        translate([0, 0, 0.75*radius]) {
            cube([2*radius, heigth, 2*radius], center=true);
            cube([heigth, 2*radius, 2*radius], center=true);
        };
    };
};

module thread_in(radius, pitch, heigth) {
    side_l = 0.5 * pitch;
    shape = shape_cyclicpolygon(3, side_l, 0);
    helix_extrude(shape,
        radius=radius,
        levels=heigth/pitch,
        level_dist=pitch);
};

module thread_out(radius, pitch, heigth) {
    side_l = 0.5 * pitch;
    shape = [ for (p=shape_cyclicpolygon(3, side_l, 0)) rotate_p(p, [0, 180, 0]) ];
    helix_extrude (shape,
        radius=radius,
        levels=heigth/pitch,
        level_dist=pitch);
};

module half_scale(radius=25, faces=8, heigth=3, angle=5, frags=2, emboss=0) {
    size = [radius, 2*radius, heigth];
    module __h() {
        intersection() {
            cube(size);
            translate([0, radius, 0]) linear_extrude(heigth)
                polygon(shape_cyclicpolygon(faces, radius, heigth));
        };
    };
    module __q() {
        intersection() {
            __h();
            translate([0, radius, 0]) __h();
        };
    };

    if (angle == 0) {
        difference() {
            __h();
            translate([0, 0,  heigth-emboss]) __q();
        };
        translate([0, 0, -emboss]) {
            __q();
        };
    } else {
         difference() {
            __h();
            __q();
        };
        translate([size.x, 0, size.z * (1 + sin(angle))])
            rotate([-90, 0, 90])
            translate([0, size.y / (2 * sin(angle/2)) - size.z * sin(angle), 0])
            bend(size=[size.y, size.x, size.z], angle=angle, frags=frags) 
            rotate([0, 0, -90]) translate([-size.x, 0, 0])
            __q();
    };
};

// -- not to be printed
module PCB() {
    union() {
        color("Green") render() {
            linear_extrude(1.4)
                union() {
                    intersection() {
                        circle(r=24);
                        translate([2, -24]) difference() {
                            square([22, 38]);
                            translate([16, 36]) circle(d=1.8);
                            translate([0, 4]) circle(d=1.8);
                        };
                    };
                    translate([25,0]) square([3.5, 9], center=true);
                };
        };
        color("Grey") 
            translate([30, 0, 0])
            rotate([-90, 0, 90])
            linear_extrude(5.8)
            offset(r=1) offset(-1) polygon([[-3.7, 3.7], [3.7, 3.7], [2.8, 0], [-2.8, 0]]);
        translate([19.5, 0, 2]) {
            color("Grey") cube([3, 4, 1.2], center=true);
            color("Black") translate([0, 0, 0.6]) cylinder(d=1.9, h=0.8);
        };
        translate([10.7, 0, 1.4]) {
            color("White")
                cube([1, 2, 0.1], center=true);
            translate([1, 0, 0])
                color("Blue")
                cube([1, 2, 0.1], center=true);
        };

        rotate([0, 0, 45])
        translate([0, 0, 2.95])
        for (a=[-1:2:1])
            translate([5.7 * a, -20.6, 0]) rotate([90, 0, 0]){
                color("White") cube([7, 3.5, 2.6], center=true);
                color("Black") translate([0.2, 0, 1.6]) cube([4, 2, 0.2], center=true);
            };
    };
};

module Speaker() {
    color("DarkGray") cylinder(d=32, h=3.5);
    translate([0, 0, 3.5]) cylinder(d=18.5, h=1.8);
};

// -- to be printed starts below

module __base(left=false) {
    difference() {
        cylinder(d=72.4, h=1.2);
        translate([0, 0, -1]) {
            cylinder(d=4, h=2.2);
            for(angle=ring(6))
                translate([6 * cos(angle), 6 * sin(angle), 0 ]) cylinder(d=4, h=2.2);
            for(angle=ring(12))
                translate([12 * cos(angle), 12 * sin(angle), 0]) cylinder(d=4, h=2.2);
        };
        translate([0, 0, 0.6]) cylinder(h=0.8, d=32.4);
    };
    translate([0, 0, 1.2]) {
        difference() {
            union() {
                translate([0, 0, -1]) thread_in(heigth=8, pitch=2, radius=26);
                cylinder(d=52, h=8);
                };
            translate([0, 0, -1]) cylinder(d=43, h=6);
            translate([0, 0, -6]) cylinder(d=56, h=5);
            translate([0, 0, 5]) cylinder(d=56, h=5);
        };
        difference(){
            cylinder(d=34, h=3);
            cylinder(d=32.4, h=3);
        };
        translate([0, 0, 2.5]) intersection() {
            difference() {
                union() {
                    for (i=[0:120:360]) {
                        rotate([0,0, i]) translate([0, -1, -0.5]) cube([17.6, 2, 1]); };
                    };
                translate([0, 0, -1]) cylinder(d1=29, d2=33, h=1.5);
            };
            translate([0, 0, -1]) cylinder(d=34, h=2);
        };
    };
};

module __stage2(left=false) {
    difference() {
        union() {
            difference() {
                cylinder(d=60, h=15);
                translate([0, 0, -1]) cylinder(d=54, h=17);
                difference() {
                    cylinder(d=62, h=4);
                    cylinder(d=58, h=4);
                };
                translate([0, 0, 12]) difference() {
                    cylinder(d=62, h=3);
                    cylinder(d=56, h=3);
                };
            };
            thread_out(radius=27, pitch=2, heigth=4);
            translate([0, 0, 11]) rotate([0, 0, 90]) thread_in(radius=28, pitch=2, heigth=6);
            if (!left) {
                rotate([0, 0, 145]) translate([25, 0, 10]) difference() {
                    cube([5, 8, 7], center=true);
                    scale(0.8) cube([5, 8, 7], center=true);
                    translate([-4, -4, 0]) cube(8);
                    translate([-2, 0, 0]) cube([2, 1, 8], center=true);
                };
                rotate([0, 0, -130]) translate([-27, 0, 0]) rotate([0, 0, 90]) {
                    mirror_copy([1, 0, 0]) translate([7, 0, 7]) {
                        translate([0, -2.5, 0]) {
                            cube([1, 5, 2.5], center=true);
                            cube([2.5, 5, 1], center=true);
                        };
                    };
                };
                hull() {
                    rotate([0, 0, -40]) mirror_copy([1, 0, 0]) translate([8, 29.5, 7]) rotate([90, 0, 0]) cylinder(d=3, h=2);
                };
            };
        };
        for(angle=ring(2))
            translate([24 * cos(angle), 24 * sin(angle), 5]) rotate([angle, 90, 0])
                linear_extrude(6.4) union() {
                    circle(d=6);
                    translate([3.6, 0]) square([7, 6], center=true);
                }
        translate([0, 0, 15]) cylinder(d=62, h=4);
        translate([0, 0, -4]) cylinder(d=62, h=4);
        if (!left) {
            rotate([0, 0, 90]) translate([26, 0, 2]) cube([6, 10, 6], center=true);
            rotate([0, 0, 50]) mirror_copy([0, 1, 0]) translate([26, 2, 7.25]) cylinder(d=1, h=3, center=true);
            translate([0, 0, 7]) rotate([-90, 0, -40]) mirror_copy([1, 0, 0]) linear_extrude(100){
                line2d([2, 2], [8, 2], width=1);
                line2d([2, -2], [8, -2], width=1);
                translate([8, 0]) rotate([0, 180, 0]) arc(radius=2, angle=[90, 270], width=1);
            };
            rotate([0, 0, 145]) translate([25, 0, 9]) rotate([0, 90, 0]) cylinder(h=5, d1=0, d2=2);
        };
        rotate_extrude() polygon([[29, 16], [27, 16], [29, 14]]);
    };
};

module __stage1(left=false) {
    difference() {
        union() {
            difference() {
                cylinder(d=63, h=6);
                cylinder(d=59, h=6);
            };
            difference() {
                linear_extrude(2) difference() {
                    polygon([ for (p=shape_cyclicpolygon(8, 35, 3)) rotate_p(p, [0, 0, 360/16])]);
                    circle(d=59);
                };
            };
        };
        for(angle=[0:180:360]) 
            translate([32 * cos(angle), 32 * sin(angle), 6]) rotate([angle, -90, 0]) cylinder(d=6, h=4);
        if(!left)
            translate([0, 32, 6]) rotate([90, 0, 0])
                linear_extrude(6) offset(r=1) offset(-1) square([7, 8.2], center=true);
    };
};

module __cap(left=false) {
    union() {
        difference() {
            linear_extrude(5.6) polygon([ for (p=shape_cyclicpolygon(8, 34, 3)) rotate_p(p, [0, 0, 360/16])]);
            translate([0, 0, 5.5]) rotate_extrude() translate([30, 0, 0]) circle(d=1);
            cylinder(d=59, h=3);
            if(!left) {
                t = 3 / ( 2 * sin (180/8));
                translate([0, 19.5, 3]) linear_extrude(4) {
                    polyline2d( points=[for (p=shape_cyclicpolygon(8, 6, 1))
                                               rotate_p(p, [0, 0, 360/16])],
                                width=1,
                                startingStyle="CAP_ROUND",
                                endingStyle="CAP_ROUND"
                                            );
                    line2d(p1=[9 , (t/2)], p2=[6, (t/2)], width=1);
                    line2d(p1=[9 , -(t/2)], p2=[6, -(t/2)], width=1);
                };
                translate([0, 9/(2*sin(180/8)) - 4.3, 0]) cube([5, 2, 8], center=true);
                translate([9, 19.5, 3]) rotate([90, 0, 0]) cylinder(d=1, h=5, center=true);
            };
            // a = 2 * r * sin(PI/8);
            // a/r = 2 * sin(PI/8);
            // 1/r = 2/a * sin(PI/8);
            r = 9 / (2 * sin(180/8));
            shape=shape_cyclicpolygon(8, r, 1);
            translate([0, -2.6, 3.5]) rotate([0, 0, 360/16]) linear_extrude(1.8) hollow_out(shell_thickness=2) polygon(shape);
            translate([0, -2.6, 4]) rotate([0, 0, 360/16]) linear_extrude(4) hollow_out(shell_thickness=1.8) polygon(shape);
        };

        difference() {
            thread_out(radius=29, pitch=2, heigth=6);
            translate([0, 0, 4]) cylinder(d=60, h=6);
            translate([0, 0, -1]) cylinder(d=60, h=1);
        };
        if(!left) { 
            translate([0, 19.5, 0.6]) union() {
                cube([1, 4, 5], center=true);
                cube([4, 1, 5], center=true);
            };
        };
    };
};

module __led(left=false) {
    r = 9 / (2 * sin(180/8));
    if(!left) {
        translate([0, 6.5, 2.6]){
            cube([5, 1, 4], center=true);
        };
    };
    translate([0, -3.5, 4]) rotate([0, 0, 360/16]) linear_extrude(1.5) hollow_out(shell_thickness=1.8) polygon(shape_cyclicpolygon(8, r*0.99, 1));
};

module __flange_bolt() {
    difference() {
        union() {
            intersection() {
                linear_extrude(6)
                polygon(shape_cyclicpolygon(8, 7, 0.5));
                sphere(r=7.5);
                cylinder(r=7, h=5);
            };
            linear_extrude(2)
            difference() {
                polygon(shape_cyclicpolygon(8, 7.5, 0.5));
                for(angle=[0:180/2:180]) {
                    rotate([0, 0, angle+360/16])
                    square([2.5, 70], center=true);
                };
            };
        };
        cylinder(r=4, h=7);
        thread_in(radius=4.2, pitch=1, heigth=8);
        translate([0, 0, -2])
        sphere(r=5);
        translate([0, 0, 6])
        sphere(r=5);
    };
}

module __flange_clip() {
    difference() {
        union() {
            intersection() {
                fillet_join(r=1, v=[0, 0, 1]) {
                    linear_extrude(1)
                    difference() {
                        intersection() {
                            translate([-4, 0])
                            square([9, 18], center=true);
                            rotate([0, 0, 360/16])
                            polygon(shape_cyclicpolygon(8, 8, 3));
                        };
                        circle(d=6);
                    };
                    multmatrix([[1, 0, 0, 0],
                                [0, 1, 0, 0],
                                [cos(360/16), 0, 1, -6],
                                [0, 0, 0, 1]])
                    linear_extrude(30)
                    difference() {
                        hull(){
                            rotate([0, 0, 360/16])
                            polygon(shape_cyclicpolygon(8, 8, 3));
                            translate([20, 0])
                            rotate([0, 0, 360/16])
                            polygon(shape_cyclicpolygon(8, 8, 3));
                        };
                        hull() {
                            rotate([0, 0, 360/16])
                            polygon(shape_cyclicpolygon(8, 6, 2));
                            translate([20, 0])
                            rotate([0, 0, 360/16])
                            polygon(shape_cyclicpolygon(8, 6, 2));
                        };
                    };
                };

                translate([-10, -10, 0])
                cube([25, 25, 26]);
            };

            rotate([90, 0, 0])
            translate([10, 34.5, 7])
            union() {
                linear_extrude(3, center=true) 
                hull() {
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 8, 2));

                    translate([0, -11.2, 0])
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 8, 2));
                };
                linear_extrude(2.5)
                rotate([0, 0, -45])
                intersection() {
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 8, 2));
                    hull() {
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 7, 1));
                        translate([30, 0, 0])
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 7, 1));
                    };
                };
            };
        };
        translate([10, -8, 35])
        rotate([90, 0, 0]) {
            cylinder(d=6, h=6, center=true);
            rotate([0, 0, -45])
            translate([7, 0, 0]) {
                cube([14, 6, 6], center=true);
                hull() {
                    translate([-7, 0, 0])
                    cylinder(d1=6, d2=10, h=2);
                    translate([7, 0, 0])
                    cylinder(d1=6, d2=10, h=2);
                };
            };
            translate([12, 0, 0])
            rotate([0, 0, 45]) {
                cube([14, 6, 6], center=true);
                hull() {
                    translate([-7, 0, 0])
                    cylinder(d1=6, d2=10, h=2);
                    translate([7, 0, 0])
                    cylinder(d1=6, d2=10, h=2);
                };
            };
        };
    };
};

module __flange_top() {
    fillet_join(r=0.7, v=[0, 0, 1.4]) {
        rotate([0, 0, 90])
        difference() {
            minkowski(){
                linear_extrude(1, center=true)
                hull(){
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 5, 1.5));
                    translate([34, 0])
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 5, 1.5));
                };
                sphere(r=1);
            };
            translate([-5, -1, 0])
            cylinder(d=2, h=6, center=true);
        };
        cylinder(r=3, h=10);
    };
    translate([0, 0, 4])
    intersection() {
        union() {
            thread_in(radius=4, pitch=1, heigth=8);
            cylinder(h=8, r=3.75);
        };
        translate([0, 0, 4])
        scale([1, 1, 0.8])
        sphere(d=9.5);
        cylinder(r=4.3, h=8);
    };
    translate([0, 34, -1])
    intersection() {
        sphere(r=3, $fn=8);
        translate([0, 0, -1])
        cube([6, 6, 5], center=true);
    };
};

module __flange() {
    difference() {
        union() {
            // Ear shape
            difference() {
                union() {
                    hull() {
                        linear_extrude(0.1)
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 36, 4));

                        translate([4, 10, 3])
                        linear_extrude(0.1)
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 50, 4));

                        translate([4, 10, 7])
                        linear_extrude(0.1)
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 50, 4));

                        translate([0, 0, 10])
                        linear_extrude(0.1)
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 36, 4));
                    };

                    // The long rod out of the ear
                    translate([-5, 20+80, 5])
                    rotate([90, 0, 0])
                    multmatrix([[1, 0, 0, 0],
                                [0, 1, 0, 0],
                                [cos(360/16), 0, 1, 0],
                                [0, 0, 0, 1]])
                    linear_extrude(80)
                    hull(){
                        translate([5, 0])
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 6, 2));

                        translate([25, 0])
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 6, 2));
                    };

                };
                // Speaker's space
                translate([0, 0, -4])
                linear_extrude(20)
                rotate([0, 0, 360/16])
                polygon(shape_cyclicpolygon(8, 36, 4));

                // Vertical hole
                translate([-5, 40+100, 5])
                rotate([90, 0, 0])
                linear_extrude(100)
                    hull() {
                        translate([8, 0])
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 3, 1.5));

                        translate([22, 0])
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 3, 1.5));
                };

                // The non needed half, plus a octogonal rod.
                difference() {
                    translate([-50, 8, 0])
                    cube([100, 200, 100], center=true);

                    translate([0, 0, 5])
                    rotate([90, 0, 0])
                    linear_extrude(220, center=true)
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 6, 2));
                };
            };

            // The rod around which the ear will rotate
            translate([0, 34, 5])
            rotate([90, 90, 0])
            difference() {
                cylinder(d=5, h=70);
                translate([-5, -1, 0])
                cube([5, 2, 70]);
            };

            // The top shoulder
            translate([0, 40, 5])
            rotate([90, 0, 0])
            translate([-5, 0, 0])
            multmatrix([[1, 0, 0, 0],
                        [0, 1, 0, 0],
                        [cos(360/16), 0, 1, -80],
                        [0, 0, 0, 1]])
            linear_extrude(20)
            difference() {
                hull(){
                    translate([5, 0])
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 8, 2));
                    translate([25, 0])
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 8, 2));
                };
                hull() {
                    translate([8, 0])
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 3, 1.5));
                    translate([22, 0])
                    rotate([0, 0, 360/16])
                    polygon(shape_cyclicpolygon(8, 3, 1.5));
                };
            };
        };

        // The gorge to clip the headband in place
        translate([10, 62, 9])
        rotate([0, 0, 90])
        linear_extrude(4, center=true)
        hull(){
            rotate([0, 0, 360/16])
            polygon(shape_cyclicpolygon(8, 6, 1.5));
            translate([100, 0])
            rotate([0, 0, 360/16])
            polygon(shape_cyclicpolygon(8, 6, 1.5));
        };

        // wire way
        path = [[0, 33.75, 5],
                [0, 33.75, 10],
                [0, 42, 10],
                [12, 58, 10]];
        polyline3d(points=path, thickness=2, $fn=24);

        // The wire way, on the top shoulder
        translate([8, 80, 7])
        rotate([0, -45, 0])
        cube([1.5, 50, 10]);
    };
};

module __headband_bottom() {
    difference() {
        union() {
            // Twist the part
            multmatrix([[1, 0, 0, 0],
                        [0, 1, 0, 0],
                        [-cos(360/16), 0, 1, 45],
                        [0, 0, 0, 1]])
            // Fillet between the shoulder and the bunny ear shaped part
            fillet_join(r=1) {
                // First shoulder, filleted with the
                // insert for the headband
                rotate([0, 180, 0])
                difference() {
                    fillet_join(r=1, v=[0, 0, 1]) {
                        linear_extrude(10)
                        hull(){
                            rotate([0, 0, 360/16])
                            polygon(shape_cyclicpolygon(8, 10, 2));
                            translate([20, 0])
                            rotate([0, 0, 360/16])
                            polygon(shape_cyclicpolygon(8, 10, 2));
                        };
                        union() {
                            linear_extrude(30)
                            __profile_headband_bottom();
                            translate([-5, 0, 15])
                            sphere(d=2);
                        };
                    };
                    translate([10, -21, 30])
                    cube([1, 20, 50]);
                };
                // Bunny hear shaped part
                rotate([0, 180, 180])
                translate([-20, 0, -40])
                linear_extrude(40)
                __profile_bunny_ear();
            };
            // Let's close the loop
            translate([0, 0, 85])
            rotate([0, 0, 180])
            linear_extrude(20)
            __profile_bunny_ear();

            // Bunny ears are supposed to be round
            translate([-10, 0, 105])
            rotate([90, 0, 0])
            rotate_extrude(angle=180)
            translate([-10, 0, 0])
            intersection() {
                __profile_bunny_ear();
                translate([10, -10])
                square(20);
            };
        };
        // We need a space for the wire in the back
        translate([-5, 0, 0])
        rotate([0, 0, 45])
        cube([1.5, 20, 130]);
        translate([-10, 5, 120])
        rotate([0, 0, -45])
        cube([20, 1.5, 20]);
        // And one in the front
        translate([-10, -10.5, 105])
        union() {
            cube([1.5, 10, 10]);
            translate([-6, 0, 10])
            rotate([0, 45, 0])
            cube([10, 10, 10]);
        };
    };
};

module __headband_top() {
    mirror_copy(v=[-1, 0, 0])
    union() {
        translate([90 * cos(-30), 90 * sin(-30), 0])
        rotate([-90, 90, -30])
        translate([-10, 0, -30])
        difference() {
            multmatrix([[1, 0, 0, 0],
            [0, 1, 0, 0],
            [-cos(360/16), 0, 1, 0],
            [0, 0, 0, 1]])
            union() {
                linear_extrude(20) __profile_headband_top();
                translate([0, 0, 20])
                linear_extrude(2)
                difference() {
                    hull() {
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 9, 2));

                        translate([20, 0])
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 9, 2));
                    };
                    translate([10, 0])
                    square([2, 10]);
                };

                translate([0, 0, 22])
                linear_extrude(40)
                difference() {
                    hull() {
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 6, 2));

                        translate([20, 0])
                        rotate([0, 0, 360/16])
                        polygon(shape_cyclicpolygon(8, 6, 2));
                    };
                    translate([10, 4.9])
                    circle(d=2);
                };
            };
            translate([-5, 0, 15]) sphere(2.5);
            translate([0, 0, 30])
            linear_extrude(45)
            square(100, center=true);
        };
        rotate([0, 0, -30])
        rotate_extrude(angle=120)
        translate([90, 0, 0])
        difference() {
            hull() {
                translate([0, -10])
                rotate([0, 0, 360/16])
                polygon(shape_cyclicpolygon(8, 6, 2));

                translate([0, 10])
                rotate([0, 0, 360/16])
                polygon(shape_cyclicpolygon(8, 6, 2));
            };
            translate([-4.9, 0])
            circle(d=2);
        };
    };
};

module __profile_bunny_ear() {
    difference() {
        hull() {
            translate([3.25, 0])
            rotate([0, 0, 360/16])
            polygon(shape_cyclicpolygon(8, 3, 2));
            translate([16.75, 0])
            rotate([0, 0, 360/16])
            polygon(shape_cyclicpolygon(8, 3, 2));
        };
        translate([10, 4, 0])
        rotate([0, 0, 360/16])
        polygon(shape_cyclicpolygon(8, 4, 1));
    };
};

module __profile_headband_top() {
    difference() {
        hull() {
            rotate([0, 0, 360/16])
            polygon(shape_cyclicpolygon(8, 9, 2));

            translate([20, 0])
            rotate([0, 0, 360/16])
            polygon(shape_cyclicpolygon(8, 9, 2));
        };

        hull() {
            rotate([0, 0, 360/16])
            polygon(shape_cyclicpolygon(8, 6.5, 2));
            translate([20, 0])
            rotate([0, 0, 360/16])
            polygon(shape_cyclicpolygon(8, 6.5, 2));
        };

        translate([10, 0])
        square([2, 10]);
    };
};

module __profile_headband_bottom() {
    difference() {
        difference() {
            hull() {
                rotate([0, 0, 360/16])
                polygon(shape_cyclicpolygon(8, 6, 2));
                translate([20, 0])
                rotate([0, 0, 360/16])
                polygon(shape_cyclicpolygon(8, 6, 2));
            };

            hull() {
                rotate([0, 0, 360/16])
                polygon(shape_cyclicpolygon(8, 4, 2));
                translate([20, 0])
                rotate([0, 0, 360/16])
                polygon(shape_cyclicpolygon(8, 4, 2));
            };
        };
    };
};

module part_headband_bottom_left() {
    __headband_bottom();
}

module part_headband_bottom_right() {
    mirror([1, 0, 0])
    __headband_bottom();
};

module part_headband_top() {
    __headband_top();
};

module part_flange_bolt() {
    __flange_bolt();
};

module part_flange_left() {
    __flange();
};

module part_flange_right() {
    mirror([0, 0, 1])
    __flange();
};

module part_flange_clip_left() {
    mirror([0, 0, 1])
    __flange_clip();
};

module part_flange_clip_right() {
    __flange_clip();
};

module part_flange_top() {
    __flange_top();
};

module part_cap_left() {
    __cap(left=true);
};

module part_stage2_left() {
    __stage2(left=true);
};

module part_stage1_left() {
    __stage1(left=true);
};

module part_base_left() {
    __base(left=true);
};

module part_led_left() {
    __led(left=true);
};

module part_cap_right() {
    __cap(left=false);
};

module part_stage2_right() {
    __stage2(left=false);
};

module part_stage1_right() {
    __stage1(left=false);
};

module part_base_right() {
    __base(left=false);
};

module part_led_right() {
    __led(left=false);
};

//    color("DimGray", alpha=0.8) render() part_flange_right();
//    color("DimGray", alpha=0.1) render() part_flange_left();
//    render() part_flange_clip_right();
    translate([10, -10, 100])
    rotate([-90, 0, 180])
color("Red") render() part_flange_top();
//    color("Silver") render() part_flange_bolt();
    
//translate([0, 80, -5])
//rotate([-90, 180, 0])
color("Red") render() part_headband_bottom_right();
//translate([0, 0, 55]) render() part_headband_top();
//color("DimGray", alpha=0.6) render() part_headband_top();
