# headphones

Those headphones part are meant to replace the components of the one I got, and
to improve over the current design which requires to cut the cable linking the
speakers in order to be disassembled.

For now it is meant to be used with the electronic board and speakers
of the original headphone.

There's no need to cut wires anymore, the headphones can be taken appart without
any tool, to replace broken parts, or to improve/change the design.