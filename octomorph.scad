include <dotSCAD/src/bezier_curve.scad>;
include <dotSCAD/src/along_with.scad>;
include <dotSCAD/src/shape_cyclicpolygon.scad>;
include <dotSCAD/src/rotate_p.scad>;
include <dotSCAD/src/polysections.scad>;
include <dotSCAD/src/path_extrude.scad>;

module sucker(d=5) {
    scale([1, 1, 0.8]) rotate_extrude($fn=20) translate([d/2, 0, 0]) circle(d=d);
}

function section(d=5) = shape_cyclicpolygon(
        sides=8,
        circle_r=4*d,
        corner_r=d,
        $fn=20
        );

function scale_down(inc=0.01, size=5, len=51, scale=0.2) = [ for (s=[0:scale/len:scale]) ceil(size*s/inc)];

echo(scale_down());

t_step = 0.02;
p0 = [0,0, 0];
p1 = [0, 50, 35];
p2 = [-100, 0, 0];
p3 = [30, 120, -35];
p4 = [30, 150, -40];
p5 = [0, 200, -3];

//path_pts = bezier_curve(t_step, [p0, p1, p2, p3, p4, p5]);
//path_extrude(section(), path_pts, scale=[0.2, 0.1]);

//#for (p=[p0, p1, p2, p3, p4, p5]) translate(p) sphere(5);

for (i=[0:7]) {
    echo(360*i/8);
    translate([40*cos(360*i/8), 40*sin(360*i/8), 0]) polygon(shape_cyclicpolygon(
        sides=8,
        circle_r=20,
        corner_r=5,
        $fn=20
        )
    );
};
translate([0, 0, 10]) polygon(shape_cyclicpolygon(sides=8, circle_r=40, corner_r=5, $fn=20));
translate([0, 0, 30]) rotate([20, 0, 0]) polygon(shape_cyclicpolygon(sides=8, circle_r=50, corner_r=5, $fn=20));
translate([0, -30, 80]) rotate([45, 0, 0])polygon(shape_cyclicpolygon(sides=8, circle_r=70, corner_r=5, $fn=20)); 
translate([0, -65, 90]) rotate([60, 0, 0])polygon(shape_cyclicpolygon(sides=8, circle_r=50, corner_r=5, $fn=20)); 
translate([0, -80, 100]) rotate([60, 0, 0])polygon(shape_cyclicpolygon(sides=8, circle_r=20, corner_r=5, $fn=20)); 
